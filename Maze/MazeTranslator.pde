//by Manuel Hanifl
class MazeTranslator { //<>//
    float tileSize;
    int[][] maze;
    
    MazeTranslator(float tileSize, int[][] maze) {
        this.tileSize = tileSize;
        this.maze = maze;
        //buildMaze();
    }
    
    
    void import_maze() {
        StringList array = new StringList();
        char[] temp;
        IntList temp_line = new IntList();
        BufferedReader reader = createReader("C:\\Users\\Manuel\\Desktop\\Processing\\Labyrinth\\maze-big.txt");
        String line = null; 
        try { 
            while ((line = reader.readLine()) != null) { 
                /* 
                 String[] pieces = split(line, ""); 
                 text[counter] = pieces;   
                 counter++;*/
                array.append(split(line, "\n"));
            }
            reader.close();
            int size = array.size(); //<>//
            maze = new int[size][];
            for (int i = 0; i < array.size(); i++) {
                array.set(i, array.get(i).replace("{", "")); //<>//
                array.set(i, array.get(i).replace("}", ""));
                array.set(i, array.get(i).replace(",", ""));
                array.set(i, array.get(i).replace(" ", ""));
                temp = array.get(i).toCharArray();
                for (int j = 0; j < temp.length; j++) {
                    temp_line.append(int(temp[j]));
                }
                maze[i] = new int[temp_line.size()];
                for (int j = 0; j < maze[i].length; j++) {
                    maze[i][j] = temp_line.get(j);
                }
            }
        }  
        catch (IOException e) {
            e.printStackTrace();
        }
    }



    void buildMaze() {
        maze = new int[800][];
        for (int i = 0; i < maze.length; i++) {
            maze[i] = new int[1200];
        }
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                int rand = int(random(0, 2));   
                if (rand == 0) {
                    maze[i][j] = 0;
                } else {
                    maze[i][j] = 1;
                }
            }
        }
    }



    void show() {
        stroke(0);
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                if (maze[i][j] == 1) {
                    stroke(100);
                    fill(100);
                } else if (maze[i][j] == 2) { //Goal
                    stroke(100, 255, 50);
                    fill(100, 255, 50);
                } else if (maze[i][j] == 9) {
                    stroke(220, 100, 50);
                    fill(220, 100, 50);
                } else if (maze[i][j] == 8) {
                    stroke(50, 100, 220);
                    fill(50, 100, 220);
                } else if (maze[i][j] == 10) {
                    stroke(220, 20, 70);
                    fill(220, 20, 70);
                } else {
                    stroke(255);
                    fill(255);
                }
                rect(j * tileSize, i * tileSize, tileSize, tileSize);
            }
        }
    }

    int[][] getMaze() {
        int[][] temp = maze;
        return temp;
    }
}
