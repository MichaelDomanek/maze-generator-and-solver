//by Manuel Hanifl
//current_best: brain_tremaux
//issues: two successive intersections shifted by one (just hope it doesn't happen)


class Player {                
    PVector pos;
    PVector vel;
    PVector facing_dir;
    float size;
    int[][] maze;
    String dirs[] = {"up", "down", "right", "left"};
    int crossings;
    color c;


    Player(float size, int[][] maze, color c) {
        pos = new PVector(1, 0);
        vel = new PVector(0, 0);
        facing_dir = new PVector(0, 0);
        crossings = 0;
        this.size = size; //<>// //<>//
        this.maze = maze;
        this.c = c;
    }

    void show() {
        fill(c);
        stroke(c);
        //fill(0);
        rect(pos.x * size, pos.y * size, size, size);
    }

    void changeVel(String direction) {
        if (direction == "up") {
            vel.set(0, -1);
        } else if (direction == "down") {
            vel.set(0, 1);
        } else if (direction == "right") {
            vel.set(1, 0);
        } else if (direction == "left") {
            vel.set(-1, 0);
        }
    }


    boolean check_move(String direction) {
        changeVel(direction); //<>// //<>//
        if ((pos.x + vel.x < 0 || pos.y + vel.y < 0) || (pos.x + vel.x > maze[0].length - 1 || pos.y + vel.y > maze.length - 1)) {
            return false;
        }
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 1 || maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 9) {
            return false;
        } else {
            return true;
        }
    }

    String[] check_dir() {
        String dirs[] = {"up", "down", "right", "left"};
        StringList array  = new StringList();
        for (int i = 0; i < dirs.length; i++) {
            if (check_move(dirs[i])) {
                array.append(dirs[i]);
            }
        }
        return array.array();
    }

    boolean check_mark(String direction) {
        changeVel(direction);
        if ((pos.x + vel.x < 0 || pos.y + vel.y < 0) || (pos.x + vel.x > maze[0].length - 1 || pos.y + vel.y > maze.length - 1)) {
            return false;
        }
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 9 || maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 8) {
            return true;
        } else {
            return false;
        }
    }

    String[] check_marks() {   
        StringList array  = new StringList();
        for (int i = 0; i < dirs.length; i++) {
            if (check_mark(dirs[i])) {
                array.append(dirs[i]);
            }
        }
        return array.array();
    }

    boolean check_known_path(String direction) {
        changeVel(direction);
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 8) {
            return true;
        } else if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 8) {
            return true;
        } else {
            return false;
        }
    }

    void brain_old() {

        String[] dirs = check_dir();

        if (dirs.length > 2) {
            //maze[int(pos.y)][int(pos.x)] = 9;
        } else {
            //maze[int(pos.y)][int(pos.x)] = 8;
        }
        if (dirs.length > 2) {
            int decision = (int)random(0, dirs.length);
            move(dirs[decision]);
        } else if (dirs.length == 1) {
            move(dirs[0]);
        } else {
            if (check_known_path(dirs[0])) {
                move(dirs[1]);
            } else {
                move(dirs[0]);
            }
        }
    }

    void mark(int x, int y) {
        if (maze[x][y] == 8) {
            maze[x][y] = 9;
        } else {
            maze[x][y] = 8;
        }
    }

    void inverseFacing() {
        facing_dir.set(facing_dir.x * -1, facing_dir.y * -1);
    }


    String translate(PVector p) {
        if (p.x == 0 && p.y == -1) {
            return "up";
        } else if (p.x == 0 && p.y == 1) {
            return "down";
        } else if (p.x == 1 && p.y == 0) {
            return "right";
        } else {
            return "left";
        }
    }

    int check_mark_rank(String direction) {
        changeVel(direction);
        if ((pos.x + vel.x < 0 || pos.y + vel.y < 0) || (pos.x + vel.x > maze[0].length - 1 || pos.y + vel.y > maze.length - 1)) {
            return 10;
        }
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 9) {
            return 9;
        } else if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 8) {
            return 8;
        } else if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 0) {
            return 0;
        }
        return 10;
    }

    String check_min_mark_rank() {
        int min = 10; 
        int index = 0; 
        for (int i = 0; i < 4; i++) {
            if (check_mark_rank(dirs[i]) < min) {
                min = check_mark_rank(dirs[i]); 
                index = i;
            } 
        }
        return dirs[index]; 
    }

    boolean check_first_mark(String direction) {
        changeVel(direction);
        if ((pos.x + vel.x < 0 || pos.y + vel.y < 0) || (pos.x + vel.x > maze[0].length - 1 || pos.y + vel.y > maze.length - 1)) {
            return false;
        }
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 8) {
            return true;
        } else {
            return false;
        }
    }

    int check_mark_sum() {
        int sum = 0;
        int temp;
        for (int i = 0; i < 4; i++) {
            temp = check_mark_rank(dirs[i]);
            if (temp != 10) {
                sum += temp;
            }
        }
        return sum;
    }

    boolean check_wall(String direction) {
        changeVel(direction);
        if ((pos.x + vel.x < 0 || pos.y + vel.y < 0) || (pos.x + vel.x > maze[0].length - 1 || pos.y + vel.y > maze.length - 1)) { //<>// //<>//
            return true;
        }
        if (maze[int(pos.y + vel.y)][int(pos.x + vel.x)] == 1) {
            return true; 
        } else {
            return false;
        }
    }

    int check_walls_count() {
        int count = 0;
        for (int i = 0; i < 4; i++) {
            if (check_wall(dirs[i])) {
                count++;
            }
        }
        return count;
    }
 
    void brain_tremaux() { 
        String[] dirs = check_dir(); 
        String[] marks = check_marks();
        int wall_count = check_walls_count();
        if ((dirs.length == 3 || marks.length >= 2) && wall_count == 1) {   //crossing of 3 roads 
            if (marks.length >= 2) { //known crossing, 2 marks 
                inverseFacing();
                if (crossings > 1 || crossings == 0) {    
                    mark(int(pos.y + facing_dir.y), int(pos.x + facing_dir.x)); //<>// //<>//
                }
                //if (check_mark(translate(facing_dir))) { 
                if (check_mark_sum() != 24) {
                    String min_dir = check_min_mark_rank();   
                    move(min_dir);
                    wall_count = check_walls_count();
                    if (wall_count > 1) {
                        mark(int(pos.y), int(pos.x));
                    } 
                } else {
                    move(translate(facing_dir));  
                    mark(int(pos.y), int(pos.x));
                }
                /*
                if (check_mark(dirs[0])) { 
                    if (check_mark(dirs[1])) {
                        move(dirs[2]);  //<>// //<>//
                        mark(int(pos.y), int(pos.x)); 
                    } else {
                        move(dirs[1]);
                        mark(int(pos.y), int(pos.x));
                    }
                } else { 
                    move(dirs[0]); 
                    mark(int(pos.y), int(pos.x));
                }*/
            } else {  //unknown crossing
                int decision = (int)random(0, dirs.length); 
                inverseFacing();
                if (crossings > 1 || crossings == 0) {    
                    mark(int(pos.y + facing_dir.y), int(pos.x + facing_dir.x));  
                } //<>// //<>//
                if (check_mark(dirs[decision])) { 
                    int new_dec = (int)random(0, dirs.length);
                    while (new_dec == decision) {
                        new_dec = (int)random(0, dirs.length);
                    } 
                    decision = new_dec; 
                } 
                move(dirs[decision]);
                wall_count = check_walls_count();
                if (wall_count > 1) { 
                    mark(int(pos.y), int(pos.x));
                }
            }
            crossings = 0;
        } else if (dirs.length == 4 || wall_count == 0) {
            if (marks.length >= 2) {
                inverseFacing();
                if (crossings > 1 || crossings == 0) {    
                    mark(int(pos.y + facing_dir.y), int(pos.x + facing_dir.x));
                }
                int sum = check_mark_sum();
                if (sum != 24 && sum != 33) {
                    String min_dir = check_min_mark_rank();  
                    move(min_dir);
                    mark(int(pos.y), int(pos.x)); 
                } else {
                    move(translate(facing_dir));
                    wall_count = check_walls_count();
                    if (wall_count > 1) {
                        mark(int(pos.y), int(pos.x));
                    }
                }
            } else { //unknown crossing
                int decision = (int)random(0, dirs.length); 
                inverseFacing();
                if (crossings > 1 || crossings == 0) {    
                    mark(int(pos.y + facing_dir.y), int(pos.x + facing_dir.x));  
                }
                if (check_mark(dirs[decision])) { 
                    int new_dec = (int)random(0, dirs.length);
                    while (new_dec == decision) {
                        new_dec = (int)random(0, dirs.length);
                    } 
                    decision = new_dec; 
                }
                move(dirs[decision]);
                wall_count = check_walls_count();
                if (wall_count > 1) { 
                    mark(int(pos.y), int(pos.x));
                }
            }
            crossings = 0;
        } else if (dirs.length == 1) {
            if (marks.length == 0) {
                crossings = 0;
            } else {
                crossings++;   
            }
            move(dirs[0]);  
        //} else if (dirs.length == 2 && marks.length < 2) {
        }  else if (dirs.length == 2) {  
            inverseFacing();    
            if (translate(facing_dir) == dirs[0]) { 
                move(dirs[1]); 
            } else {  
                move(dirs[0]);
            }
            crossings++; 
        }  
    }

    boolean check() {
        return maze[int(pos.y)][int(pos.x)] == 2;
    }

    void move(String direction) {
        if (check_move(direction)) {
            pos.add(vel);
            facing_dir.set(vel);
            /*
                String[] dirs;
            dirs = check_dir();
            print("you can go ");
            for (int i = 0; i < dirs.length; i++) {
            print(dirs[i] + " ");
            }
            println("");*/
            //println("( " + pos.x + " / " + pos.y + " );");
        }
    }
}
