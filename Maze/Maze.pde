//by Michael Domanek and Manuel Hanifl
import java.io.File;
import java.time.LocalDate;

MazeTranslator m;
Player[] p;
MazeGenerator mg;
float tileSize;
int PlayerSize;
long startTime;
long endTime;
boolean running = true;

void setup() {
    //size(300, 300);
    background(10);
    fullScreen();
    //=====variables=====
    tileSize = 2;
    PlayerSize = 1000;
    //===================
    startTime = System.nanoTime(); 
    mg = new MazeGenerator(int(tileSize));
    mg.generate();
    endTime = System.nanoTime();
    println((endTime - startTime) / 1000000, "milliseconds for generation");
    m = new MazeTranslator(tileSize, mg.getMaze());
    p = new Player[PlayerSize];
    for (int i = 0; i < p.length; i++) {
        p[i] = new Player(tileSize, mg.getMaze(), color(int(random(10, 245)), int(random(0, 245)), int(random(0, 245))));
    }
    frameRate(1000);
    m.show();
}


void draw() {
    if(running){
        for (Player pl : p) {
            pl.show();
            pl.brain_tremaux();
            /*if (pl.check()) {
                pl.show();
                makePicture();
                delay(5000);
                endTime = System.nanoTime();
                println((endTime - startTime) / 1000000, "milliseconds for solving");
                exit();    
            }*/
        }
    }
}

void makePicture(){
    String part = str(width) + "x" +str(height) + " " + str(int(tileSize)) + "tile " + str(PlayerSize) + "player " + LocalDate.now().toString();
    saveFrame(".\\Pictures\\Maze##### " + part + ".png");
}
void keyPressed() {
    if (key == ' ') {
        if(running){
            running = false; 
        } else{
            running = true;
        }
    } else if(key == 'p'){
        makePicture();
    }
    /*
    if (keyCode == UP) {
        p.move("up");   
    } else if (keyCode == DOWN) {
        p.move("down");
    } else if (keyCode == RIGHT) {
        p.move("right");
    } else if (keyCode == LEFT) {
        p.move("left");
    }
    */   
}
